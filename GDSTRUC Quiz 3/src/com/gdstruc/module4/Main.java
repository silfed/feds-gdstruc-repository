package com.gdstruc.module4;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Random rand = new Random();
        Scanner scanner = new Scanner(System.in);
        ArrayQueue playerQueue = new ArrayQueue(7);
        ArrayQueue matchQueue = new ArrayQueue(7);
        int gameRounds = 0;


        playerQueue.add(new Player(1, "Thranduil", 100));
        playerQueue.add(new Player(2, "Tauriel", 100));
        playerQueue.add(new Player(3, "Mithrandir", 95));
        playerQueue.add(new Player(4, "Legolas", 90));
        playerQueue.add(new Player(5, "Thrandur", 90));
        playerQueue.add(new Player(6, "Sauron", 90));
        playerQueue.add(new Player(7, "Legolas", 90));

        while (gameRounds < 10)
        {
            int randChoice = rand.nextInt(6) + 1;
            System.out.println("\nGames successfully made: " + gameRounds);
            System.out.println("Now queuing: " + randChoice + " players");
            scanner.nextLine();
            if (randChoice > 4)
            {
                for (int i = 0; i < randChoice; i++)
                {
                    matchQueue.matchAdd(playerQueue, matchQueue);
                }
                System.out.println("Match found! Here are your contenders: \n");
                matchQueue.printQueue();
            }
            else if (randChoice <= 4)
            {
                System.out.println("Not enough players, requeuing...\n");
                continue;
            }

            if (matchQueue.size() > 4)
            {
                for (int i = matchQueue.size(); i > 0; i--)
                {
                    playerQueue.reset(matchQueue, playerQueue);
                }
                gameRounds++;
            }
        }
    }
}
