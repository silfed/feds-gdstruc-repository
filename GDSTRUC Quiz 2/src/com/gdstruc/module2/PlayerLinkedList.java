package com.gdstruc.module2;

public class PlayerLinkedList {
    private PlayerNode head;
    public PlayerNode temp;

    public void addToFront(Player player)
    {
        PlayerNode playerNode = new PlayerNode(player);
        playerNode.setNextPlayer(head);
        head = playerNode;
    }

    public void printList()
    {
        PlayerNode current = head;
        System.out.print("HEAD -> ");
        while (current != null) {
            System.out.print(current);
            System.out.print(" -> ");
            current = current.getNextPlayer();
        }
        System.out.println("null");
    }

    public PlayerNode removeFirst()
    {
        if (head == null){
            return null;
        }

        temp = head;
        head = head.getNextPlayer();
        temp.setNextPlayer(null);

        return temp;
    }

    public int listSize()
    {
        int size = 0;
        PlayerNode current = head;

        while(current != null)
        {
            size++;
            current = current.getNextPlayer();
        }
        return size;
    }

    public int ownIndexOf(Player player)
    {
        int i = 0;
        PlayerNode current = this.head;

        while(current != null)
        {
            if(current.getPlayer().equals(player))
            {
                return i;
            }
            else {
                i++;
                current = current.getNextPlayer();
            }
        }

        return -1;
    }

    public boolean ownContains(Player player)
    {
        if(this.ownIndexOf(player) != -1)
        {
            return true;
        }
        else
            {
            return false;
        }
    }
}
