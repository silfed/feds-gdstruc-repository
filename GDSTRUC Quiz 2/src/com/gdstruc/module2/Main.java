package com.gdstruc.module2;

public class Main {

    public static void main(String[] args) {

        Player tauriel = new Player(1, "Tauriel", 383);
        Player thranduil = new Player(2, "Thranduil", 612);
        Player mithrandir = new Player(3, "Mithrandir", 562);
        Player balrog = new Player(4, "Balrog", 362);
        Player sauron = new Player(5, "Sauron", 1000);
        Player saruman = new Player(6, "Saruman", 252);

        PlayerLinkedList playerLinkedList = new PlayerLinkedList();

        playerLinkedList.addToFront(tauriel);
        playerLinkedList.addToFront(thranduil);
        playerLinkedList.addToFront(mithrandir);
        playerLinkedList.addToFront(balrog);
        playerLinkedList.addToFront(sauron);
        playerLinkedList.addToFront(saruman);

        playerLinkedList.printList();
        System.out.println(playerLinkedList.listSize());

        playerLinkedList.removeFirst();
        playerLinkedList.printList();

        System.out.println("What is the index of Player Balrog?");
        System.out.println("Index is : " + playerLinkedList.ownIndexOf(balrog));
        //should return index of 1 as we have removed Saruman as head of the Linked List

        System.out.println("Contains player id:5, name:Sauron, level:1000? " + playerLinkedList.ownContains(sauron));
        //should be true for sauron is still in our list

        System.out.println("Contains player id:6, name:Saruman, level:252? " + playerLinkedList.ownContains(saruman));
        //should be false because we have applied the removeFirst function, which removes Saruman from the list as he is in the front of the list

    }
}
