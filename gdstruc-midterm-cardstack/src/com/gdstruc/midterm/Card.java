package com.gdstruc.midterm;

import java.util.Objects;

public class Card {

    private int cardNumber;
    private String cardSuit;

    public Card(int cardNumber, String cardSuit) {
        this.cardNumber = cardNumber;
        this.cardSuit = cardSuit;
    }

    public int getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(int cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardSuit() {
        return cardSuit;
    }

    public void setCardSuit(String cardSuit) {
        this.cardSuit = cardSuit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return cardNumber == card.cardNumber &&
                Objects.equals(cardSuit, card.cardSuit);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cardNumber, cardSuit);
    }

    @Override
    public String toString() {
        return "Card{" +
                "cardNumber=" + cardNumber +
                ", cardSuit='" + cardSuit + '\'' +
                '}';
    }
}
