package com.gdstruc.midterm;
import java.util.Collection;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void clearScreen()
    {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    public static void main(String[] args) {
        CardStack deck = new CardStack();
        CardStack onHand = new CardStack();
        CardStack discard = new CardStack();
        Random rand = new Random();
        Scanner scanner = new Scanner(System.in);

        deck.push(new Card(1,"Ace of Hearts"));
        deck.push(new Card(2,"Hearts"));
        deck.push(new Card(3,"Hearts"));
        deck.push(new Card(4,"Hearts"));
        deck.push(new Card(5,"Hearts"));
        deck.push(new Card(6,"Hearts"));
        deck.push(new Card(7,"Hearts"));
        deck.push(new Card(8,"Hearts"));
        deck.push(new Card(9,"Hearts"));
        deck.push(new Card(10,"Hearts"));
        deck.push(new Card(1,"Ace of Spades"));
        deck.push(new Card(2,"Spades"));
        deck.push(new Card(3,"Spades"));
        deck.push(new Card(4,"Spades"));
        deck.push(new Card(5,"Spades"));
        deck.push(new Card(6,"Spades"));
        deck.push(new Card(7,"Spades"));
        deck.push(new Card(8,"Spades"));
        deck.push(new Card(9,"Spades"));
        deck.push(new Card(10,"Spades"));
        deck.push(new Card(1,"Ace of Diamonds"));
        deck.push(new Card(2,"Diamonds"));
        deck.push(new Card(3,"Diamonds"));
        deck.push(new Card(4,"Diamonds"));
        deck.push(new Card(5,"Diamonds"));
        deck.push(new Card(6,"Diamonds"));
        deck.push(new Card(7,"Diamonds"));
        deck.push(new Card(8,"Diamonds"));
        deck.push(new Card(9,"Diamonds"));
        deck.push(new Card(10,"Diamonds"));

        //Shuffle deck
        Collections.shuffle(deck.getStack());

        while (!deck.isEmpty())
        {
            int xDraw = rand.nextInt(5) + 1;
            int xDiscard = rand.nextInt(5) + 1;
            int randChoice = rand.nextInt(3) + 1;

            if (randChoice == 1)
            {
                //draw x cards from deck
                System.out.println("Player will now draw " + xDraw + " card/s from the deck. \n");
                onHand.drawX(xDraw, deck, onHand);
            }

            else if (randChoice == 2)
            {
                System.out.println("Player will now discard " + xDiscard + " card/s from his/her hand. \n");
                if (onHand.isEmpty())
                {
                    System.out.println("Sorry, you've got no cards in your hand. \n");
                }

                else {
                    //discard x cards from hand
                    onHand.xDiscard(xDiscard, onHand, discard);
                }
            }

            else if (randChoice == 3)
            {
                System.out.println("Player will now get " + xDraw + " card/s from the discard pile. \n");

                if (discard.isEmpty())
                {
                    System.out.println("Sorry, there are no cards in the discard pile. \n");
                }
                else
                {
                    //draw x cards from discard pile
                    onHand.drawX(xDraw, discard, onHand);
                }
            }

            System.out.println("\n" + onHand.getStack().size() + " cards on hand.");
            onHand.printStack();

            System.out.println("\n" + discard.getStack().size() + " cards in discard pile.");
            discard.printStack();

            System.out.println("\n" + deck.getStack().size() + " cards in deck.");
            scanner.nextLine();
            clearScreen();
        }
    }
}
