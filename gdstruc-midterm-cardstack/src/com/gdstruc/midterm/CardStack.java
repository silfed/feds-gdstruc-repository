package com.gdstruc.midterm;

import java.util.LinkedList;
import java.util.ListIterator;

public class CardStack {

    private LinkedList<Card> stack;

    public CardStack()
    {
        stack = new LinkedList<Card>();
    }

    public void push(Card card)
    {
        stack.push(card);
    }

    public boolean isEmpty()
    {
        return  stack.isEmpty();
    }

    public Card pop()
    {
        return stack.pop();
    }

    public Card peek()
    {
        return stack.peek();
    }

    public LinkedList<Card> getStack() {
        return stack;
    }


    public void printStack()
    {
        ListIterator<Card> iterator = stack.listIterator();

        while(iterator.hasNext())
        {
            System.out.println(iterator.next());
        }
    }

    public CardStack drawCard(int randNum, CardStack deck, CardStack onHand)
    {
        for (int i = randNum; i >= 0; i++)
        {
            deck.pop();
            onHand.push(new Card(stack.get(randNum).getCardNumber(), stack.get(randNum).getCardSuit()));
        }
        return onHand;
    }

    public CardStack drawX(int xDraw, CardStack stack, CardStack onHand)
    {
        for (int i = 0; i < xDraw; i++)
        {
            //Draw
            Card draw = stack.peek();
            onHand.push(draw);
            //Remove from deck
            stack.getStack().remove(draw);
            if (stack.isEmpty())
            {
                break;
            }
        }
        return onHand;
    }

    public CardStack xDiscard(int xDiscard, CardStack onHand, CardStack discard)
    {
        for (int i = 0; i < xDiscard; i++)
        {
            //Discard
            Card discarded = onHand.peek();
            discard.push(discarded);
            //Remove on deck
            onHand.getStack().remove(discarded);
            if (onHand.isEmpty())
            {
                break;
            }
        }
        return discard;
    }
}
