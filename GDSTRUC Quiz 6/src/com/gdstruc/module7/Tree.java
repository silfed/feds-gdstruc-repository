package com.gdstruc.module7;

public class Tree {
    private Node root;

    public void insert(int value)
    {
        if (root == null)
        {
            root = new Node(value);
        }
        else
        {
            root.insert(value);
        }
    }

    public void traverseInOrder()
    {
        if (root != null)
        {
            root.traverseInOrder();
        }
    }

    public void traverseInOrderDescending()
    {
        if (root != null)
        {
            root.traverseInOrderDescending();
        }
    }

    public Node getMin()
    {
        return minNode(root);
    }

    public Node getMax()
    {
        return maxNode(root);
    }

    public Node minNode(Node node)
    {
        if (node.getLeftChild() != null)
        {
            return minNode(node.getLeftChild());
        }

        return node;
    }

    public Node maxNode(Node node)
    {
        if (node.getRightChild() != null)
        {
            return maxNode(node.getRightChild());
        }

        return node;
    }

    public Node get(int value)
    {
        if (root != null)
        {
            return root.get(value);
        }
        return null;
    }

}