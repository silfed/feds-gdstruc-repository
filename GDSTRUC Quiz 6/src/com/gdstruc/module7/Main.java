package com.gdstruc.module7;

import java.util.Collection;
import java.util.Collections;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Tree tree = new Tree();
        tree.insert(25);
        tree.insert(17);
        tree.insert(29);
        tree.insert(10);
        tree.insert(16);
        tree.insert(-5);
        tree.insert(68);
        tree.insert(55);

        System.out.println("Traverse in Order");
        tree.traverseInOrder();

        System.out.println("\nMin: " + tree.getMin());
        System.out.println("\nMax: " + tree.getMax());

        System.out.println("\nTraverse in Descending Order");
        tree.traverseInOrderDescending();
    }
}