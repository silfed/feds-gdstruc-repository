package com.gdstruc.module5;

public class Main {

    public static void main(String[] args) {

        Player thrandur = new Player(1, "Thrandur", 98);
        Player mithrandr = new Player(2, "Mithrandr", 102);
        Player tauriel = new Player(3, "Tauriel", 312);
        Player arwen = new Player(4, "Arwen", 390);
        Player sauron = new Player(5, "Sauron", 548);

        SimpleHashtable hashtable = new SimpleHashtable();
        hashtable.put(thrandur.getName(), thrandur);
        hashtable.put(mithrandr.getName(), mithrandr);
        hashtable.put(tauriel.getName(), tauriel);
        hashtable.put(arwen.getName(), arwen);
        hashtable.put(sauron.getName(), sauron);

        //hashtable.remove("Thrandur");
        //hashtable.remove("Mithrandr");
        //hashtable.remove("Tauriel");
        //hashtable.remove("Legolas");
        //hashtable.remove("Sauron");

        hashtable.printHashtable();

        System.out.println(hashtable.get("Sauron"));
    }
}
